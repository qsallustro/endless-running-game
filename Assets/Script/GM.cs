﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GM : MonoBehaviour
{
    public static float vertVel = 0;
    public static int coinTotal = 0;
    public static float timeTotal = 0;
    public float waittoload = 0;

    public float zScenePos = 58;

    public static float zVelAdj = 1;
    public static string lvlCompStatus="";

    public Transform bbNoPit;
    public Transform bbPitMid;
    public Transform coinObj;
    public Transform obstacleObj;
    public Transform capsuleObj;
    public int randNum;
    // Start is called before the first frame update
    void Start()
    {
        Instantiate (bbNoPit, new Vector3(0,2.17f,42),bbNoPit.rotation);
        Instantiate(bbNoPit, new Vector3(0, 2.17f, 46), bbNoPit.rotation);

        Instantiate(bbPitMid, new Vector3(0, 2.17f, 50), bbPitMid.rotation);
        Instantiate(bbPitMid, new Vector3(0, 2.17f, 54), bbPitMid.rotation);

        if (zScenePos < 120)
        {
            Instantiate(bbNoPit, new Vector3(0, 2.17f, zScenePos), bbNoPit.rotation);
            zScenePos += 4;
        }
    }

    // Update is called once per frame
    void Update()
    {
        randNum = Random.Range(0, 10);

        if (randNum < 3)
        {
            Instantiate(coinObj, new Vector3(-1, 3.17f, zScenePos), coinObj.rotation);
        }
        if (randNum > 7)
        {
            Instantiate(coinObj, new Vector3(1, 3.17f, zScenePos), coinObj.rotation);
        }
        if (randNum == 4)
        {
            Instantiate(obstacleObj, new Vector3(1, 3.17f, zScenePos), obstacleObj.rotation);
        }
        if (randNum == 5)
        {
            Instantiate(obstacleObj, new Vector3(0, 3.17f, zScenePos), obstacleObj.rotation);
        }

        if (zScenePos < 400)
        {
            Instantiate(bbNoPit, new Vector3(0, 2.17f, zScenePos), bbNoPit.rotation);
            zScenePos += 4;
        }

        timeTotal += Time.deltaTime;
        if (lvlCompStatus == "fail")
        {
            waittoload += Time.deltaTime;
        }
        if (waittoload>2)
        {
            SceneManager.LoadScene ("LEVELCOMP");
        }

    }
}
